package cn.edu.sdjzu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviderApplicationTwo {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplicationTwo.class, args);
    }
}
