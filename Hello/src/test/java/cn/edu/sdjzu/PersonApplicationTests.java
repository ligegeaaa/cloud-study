package cn.edu.sdjzu;

import cn.edu.sdjzu.mapper.PersonMapper;
import cn.edu.sdjzu.service.IPersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PersonApplicationTests {

    @Autowired
    private PersonMapper personMapper;
    @Autowired
    private IPersonService personService;

    @Test
    public void test(){
        personMapper.findAll().forEach(System.out::println);
    }

    @Test
    public void testFindAllService(){
        personService.findAll().forEach(System.out::println);
    }

}
