package cn.edu.sdjzu.mapper;


import cn.edu.sdjzu.entity.Person;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PersonMapper {
    //@Select("select person_id, person_name, phone from person"))
    List<Person> findAll();
}
