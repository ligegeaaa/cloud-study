package cn.edu.sdjzu.service;

import cn.edu.sdjzu.entity.Person;
import java.util.List;

public interface IPersonService {
    List<Person> findAll();
}
