package cn.edu.sdjzu.service.impl;

import cn.edu.sdjzu.entity.Person;
import cn.edu.sdjzu.mapper.PersonMapper;
import cn.edu.sdjzu.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PersonServiceImpl implements IPersonService {
    @Autowired
    private PersonMapper personMapper;
    @Override
    public List<Person> findAll() {
        List<Person> list = personMapper.findAll();
        return list;
    }
}
