package cn.edu.sdjzu.controller;

import cn.edu.sdjzu.entity.Person;
import cn.edu.sdjzu.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    @Autowired
    private IPersonService personService;
    @GetMapping("/getAll")
    public List<Person> findAll() {
        return personService.findAll();
    }
}
