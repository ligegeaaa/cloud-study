package cn.edu.sdjzu.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer personId;
    private String personName;
    private String phone;
}
